#! python
# -*- coding: utf-8 -*-
import os
import matplotlib.pyplot as plt
import math
from math import exp

def replacer(f1,f2):
    for s in f1:
        # s = s.replace(",",".")
        f2.write(s)
    # f3 = open(os.getcwd()+"\\data\\3.txt","w")
def spliter(f):
    a=[]
    for s in f:
        a.append([float(x) for x in s.split("	")])
    return a
        # print(float(s.split("	")[0]),float(s.split("	")[1]))
def find_sigma_J_30_KeV(SunSpectrum,GeEnergyLevels_J):
    alpha = 1./137
    Z = 33
    pi = 3.14
    A = 0.2625*10**(-44)
    sigma = []
    for i in range(0, 60):
        s = 0
        with open(os.getcwd()+"\\data\\GeEnergyLevels_J.csv","r") as GeEnergyLevels_J:
            for j in GeEnergyLevels_J:
                a = j.split(";")
                energy_of_electron = i - float(a[0]) - (923/512)
                if energy_of_electron > 1.0:
                    momentum_of_electron = (energy_of_electron**2.0 - 1.0)**0.5
                    #print Z, pi, momentum_of_electron
                    Y = float(2*pi*Z*alpha/momentum_of_electron)
                    Fermi_function = Y/(1. - exp(-Y))
                    # SIGMA = A*energy_of_electron*momentum_of_electron*Fermi_function
                    J = 0
                    if "0" in a[1] or "LE" in a[1]:
                        J+=1
                    if "1" in a[1] or "LE" in a[1]:
                        J+=3
                    SIGMA = J*A*energy_of_electron*momentum_of_electron*Fermi_function
                    s+=SIGMA
                else:
                    continue
        sigma.append(s)
    return sigma
def find_sigma(SunSpectrum,GeEnergyLevels_J):
    alpha = 1./137
    Z = 33
    pi = 3.14
    A = 0.2625*10**(-44)
    sigma = []
    for i in SunSpectrum:
        s = 0
        with open(os.getcwd()+"\\data\\GeEnergyLevels_J.csv","r") as GeEnergyLevels_J:
            for j in GeEnergyLevels_J:
                a = j.split(";")
                energy_of_electron = i[0] - float(a[0]) - (923/512)
                if energy_of_electron > 1.0:
                    momentum_of_electron = (energy_of_electron**2.0 - 1.0)**0.5
                    Y = 2.0*pi*Z*alpha/momentum_of_electron
                    Fermi_function = Y/(1. - exp(-Y))
                    # SIGMA = A*energy_of_electron*momentum_of_electron*Fermi_function
                    J = 0
                    if "0" in a[1] or "LE" in a[1]:
                        J+=1
                    if "1" in a[1] or "LE" in a[1]:
                        J+=3
                    SIGMA = J*A*energy_of_electron*momentum_of_electron*Fermi_function
                    s+=SIGMA
                else:
                    continue
        sigma.append(s)
    return sigma
def find_sigma_J_3(SunSpectrum,GeEnergyLevels_J):
    alpha = 1./137
    Z = 33
    pi = 3.14
    A = 0.2625*10**(-44)
    sigma = []
    for i in SunSpectrum:
        s = 0
        with open(os.getcwd()+"\\data\\GeEnergyLevels_J.csv","r") as GeEnergyLevels_J:
            for j in GeEnergyLevels_J:
                a = j.split(";")
                energy_of_electron = i[0] - float(a[0]) - (923/512)
                if energy_of_electron > 1.0:
                    momentum_of_electron = (energy_of_electron**2 - 1)**0.5
                    Y = 2.0*pi*Z*alpha/momentum_of_electron
                    Fermi_function = Y/(1. - exp(-Y))
                    # SIGMA = A*energy_of_electron*momentum_of_electron*Fermi_function
                    J = 3
                    # if "0" in a[1] or "LE" in a[1]:
                    #     J+=1
                    # if "1" in a[1] or "LE" in a[1]:
                    #     J+=3
                    SIGMA = J*A*energy_of_electron*momentum_of_electron*Fermi_function
                    s+=SIGMA
                else:
                    continue
        sigma.append(s)
    return sigma
# def find_sigma_by_energy_2_KeV(SunSpectrum,GeEnergyLevels):
#     alpha = 1./137
#     Z = 33
#     pi = 3.14
#     A = 0.2625*10**(-44)
#     sigma = []
#     for i in SunSpectrum:
#         s = 0
#         if (i[0] > 3.95) and (i[0] < 4.15):
#             for j in GeEnergyLevels:
#                 energy_of_electron = i[0] - j[0]
#                 if energy_of_electron > 1:
#                     momentum_of_electron = (energy_of_electron**2 - 1)**0.5
#                     Y = 2*pi*Z*alpha/momentum_of_electron
#                     Fermi_function = Y/(1 - exp(-Y))
#                     SIGMA = A*energy_of_electron*momentum_of_electron*Fermi_function
#                     s+=SIGMA
#                 else:
#                     continue
#             sigma.append(s)
#             print(i[0])
#     return sigma

if __name__ == "__main__":

    f1 = open(os.getcwd()+"\\data\\SunSpectrum.txt","r")
    f2 = open(os.getcwd()+"\\data\\GeEnergyLevels.txt","r")
    f3 = open(os.getcwd()+"\\data\\GeEnergyLevels_J.csv","r")
    SunSpectrum = spliter (f1)
    GeEnergyLevels = spliter (f2)
    GeEnergyLevels_J = f3

    sigma_by_energy = find_sigma(SunSpectrum,GeEnergyLevels_J)
    sigma_by_energy_J_3 = find_sigma_J_3(SunSpectrum,GeEnergyLevels_J)
    sigma_by_energy_J_30_KeV = find_sigma_J_30_KeV(SunSpectrum,GeEnergyLevels_J)

    sigma_and_energy = []
    energy = []
    flux = []
    sigma_by_energy_flux = []
    sigma_and_energy_flux = []
    sigma_final = 0

    for i in range(len(sigma_by_energy)):
        # print (sigma_by_energy[i])
        # print (math.log10 (sigma_by_energy[i]))
        # sigma_by_energy_ln.append( math.log1p (sigma_by_energy[i]))
        sigma_and_energy.append([SunSpectrum[i][0], sigma_by_energy[i]])
        energy.append(SunSpectrum[i][0])
        flux.append(SunSpectrum[i][1])
        s = sigma_by_energy[i]*SunSpectrum[i][1]
        sigma_final+=s
        sigma_by_energy_flux.append(s)
        sigma_and_energy_flux.append([SunSpectrum[i][0], s])

    # for i in sigma_and_energy_flux:
    #     print(i)

    # plt.plot(energy,sigma_by_energy)
    # plt.plot(energy,sigma_by_energy_J_3)
    plt.plot(sigma_by_energy_J_30_KeV)

    # plt.plot(energy,sigma_by_energy_flux)
    # plt.plot(energy,sigma_by_energy_ln)
    # plt.plot(energy,flux)
    plt.axis([0, 60, 0, 1e-38])
    plt.grid()

    plt.show()

    print(sigma_final)

#Yes, I did it!
